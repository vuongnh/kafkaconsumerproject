package vn.studynow.learninghistoryservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import vn.studynow.learninghistoryservice.payload.request.LOL2Request;
import vn.studynow.learninghistoryservice.payload.response.ApiResponse;
import vn.studynow.learninghistoryservice.service.EventTrackingService;

import javax.validation.Valid;

@RequestMapping(value = "/api/v1/tracking/event")
@RestController
public class EventController {

    @Autowired
    EventTrackingService eventTrackingService;

    @PostMapping(value = "/join-lol2")
    public ResponseEntity<?> joinLOL2(@Valid @RequestBody LOL2Request lol2Request){

        try{
            eventTrackingService.save(lol2Request);
            return new ResponseEntity<>(new ApiResponse(true,"save LOL2 JOIN event SUCCEEDED !",
                    lol2Request), HttpStatus.CREATED);
        }catch (Exception ex){
            return new ResponseEntity<>(new ApiResponse(false,"save ENTRY EXAM event FAILED !"+ "by" + ex.getCause(),
                    lol2Request),HttpStatus.CREATED);
        }

    }

}
