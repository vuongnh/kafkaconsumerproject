package vn.studynow.learninghistoryservice.service;

import vn.studynow.learninghistoryservice.payload.request.LOL2Request;

public interface EventTrackingService {

    void save(LOL2Request lol2Request);
}
