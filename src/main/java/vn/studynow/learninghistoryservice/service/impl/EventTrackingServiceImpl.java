package vn.studynow.learninghistoryservice.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vn.studynow.learninghistoryservice.constant.EventName;
import vn.studynow.learninghistoryservice.model.Event;
import vn.studynow.learninghistoryservice.payload.request.LOL2Request;
import vn.studynow.learninghistoryservice.repository.EventRepository;
import vn.studynow.learninghistoryservice.service.EventTrackingService;
import vn.studynow.learninghistoryservice.util.Utils;

@Service
public class EventTrackingServiceImpl implements EventTrackingService {

    @Autowired
    EventRepository eventRepository;

    @Override
    public void save(LOL2Request lol2Request) {
        Event event = Event.builder()
                            .userId(lol2Request.getUserId())
                            .deviceId((lol2Request.getDeviceId()))
                            .grade(lol2Request.getGrade())
                            .subjectId(lol2Request.getSubjectId())
                            .cycleZeroUid(lol2Request.getCycleZeroUid())
                            .cycleOneUid(lol2Request.getCycleOneUid())
                            .cycleTwoUid(lol2Request.getCycleTwoUid())
                            .deviceType(lol2Request.getDeviceType())
                            .version(lol2Request.getVersion())
                            .eventTimestamp(lol2Request.getEventTimestamp())
                            .code(lol2Request.getCode())
                            .eventDate(Utils.genEventDate())
                            .eventName(EventName.LOL2Event.JOIN_LOL2)
                            .build();

        eventRepository.save(event);
    }
}
