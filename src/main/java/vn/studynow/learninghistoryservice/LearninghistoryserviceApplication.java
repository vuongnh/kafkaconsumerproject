package vn.studynow.learninghistoryservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import vn.studynow.learninghistoryservice.config.ApplicationProperties;

@EnableConfigurationProperties(ApplicationProperties.class)
@SpringBootApplication
public class LearninghistoryserviceApplication {

    public static void main(String[] args) {
        SpringApplication.run(LearninghistoryserviceApplication.class, args);
        System.out.println("Project is running !");
    }

}
