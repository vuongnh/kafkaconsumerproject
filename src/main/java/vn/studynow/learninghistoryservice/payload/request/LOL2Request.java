package vn.studynow.learninghistoryservice.payload.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class LOL2Request extends CommonTrackingRequest {

    @NotNull(message = "lol2_uid in request data can not be null !")
    @JsonProperty(value = "lol2_uid")
    private String cycleTwoUid;

}
