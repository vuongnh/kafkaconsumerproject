package vn.studynow.learninghistoryservice.payload.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class CommonTrackingRequest {


    @NotNull(message = "user_id in request data can not null !")
    @JsonProperty(value = "user_id")
    private String userId;

    @NotNull(message = "device_id in request data can not null !")
    @JsonProperty(value = "device_id")
    private String deviceId;

    @NotNull(message = "grade in request data can not be null !")
    @JsonProperty(value = "grade")
    private int grade;

    @NotNull(message = "subject_id in request data can not be null !")
    @JsonProperty(value = "subject_id")
    private int subjectId;

    @NotNull(message = "lol0_uid in request data can not be null !")
    @JsonProperty(value = "lol0_uid")
    private String cycleZeroUid;

    @NotNull(message = "lol1_uid in request data can not be null !")
    @JsonProperty(value = "lol1_uid")
    private String cycleOneUid;

//    @Field(value = "lol2_uid")
//    private String cycleTwoUid;
//
//    @Field(value = "lol3_uid")
//    private String cycleThreeUid;
//
//    @Field(value = "lol4_uid")
//    private String cycleFourUid;
//
//    @Field(value = "lol6_1_uid")
//    private String cycleSixDotOneUid;
//
//    @Field(value = "lol6_2_uid")
//    private String cycleSixDotTwoUid;
//
//    @Field(value = "lol6_3_uid")
//    private String cycleSixDotThreeUid;

    @NotNull(message = "version in request data can not be null !")
    @JsonProperty(value = "version")
    private String version;

    @NotNull(message = "device_type in request data can not be null !")
    @JsonProperty(value = "device_type")
    private String deviceType;

    @NotNull(message = "event_timestamp in request data can not be null !")
    @JsonProperty(value = "event_timestamp")
    private int eventTimestamp;

    @NotNull(message = "code in request data can not be null !")
    @JsonProperty(value = "code")
    private String code;

}
