package vn.studynow.learninghistoryservice.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import vn.studynow.learninghistoryservice.model.Event;

@Repository
public interface EventRepository extends MongoRepository<Event,String> {
}
