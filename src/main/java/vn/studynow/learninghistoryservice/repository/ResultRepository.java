package vn.studynow.learninghistoryservice.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import vn.studynow.learninghistoryservice.model.Result;

@Repository
public interface ResultRepository extends MongoRepository<Result,String> {
}
