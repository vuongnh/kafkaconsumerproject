package vn.studynow.learninghistoryservice.constant;

public class EventName {


    public static class LOL2Event{

        public static final String JOIN_LOL2 = "lol2_input_click_start";
        public static final String SHOW_ENTRY_EXAM_LOL2 = "lol2_input_get_exam";
        public static final String SUBMIT_ENTRY_EXAM = "lol2_input_submit_exam";
        public static final String NEXT_LOL2_AFTER_ENTRY_EXAM= "lol2_input_click_next_lol2";
        public static final String CONTINUE_LOL2_AFTER_ENTRY_EXAM= "lol2_input_click_button_continue_practice";
        public static final String VIEW_ENTRY_EXAM_ANSWER= "lol2_input_click_button_view_detail_answer";


        public static final String FINISH_LOL2 = "lol2_output_click_start";
        public static final String SHOW_FINAL_EXAM = "lol2_output_get_exam";
        public static final String SUBMIT_FINAL_EXAM= "lol2_output_submit_exam";
        public static final String NEXT_LOL2_AFTER_FINAL_EXAM= "lol2_output_click_next_lol2";
        public static final String VIEW_FINAL_EXAM_ANSWER = "lol2_output_click_button_view_detail_answer";
    }

    public static class LOL4Event{
        public static final String JOIN_LOL4 = "lol4_click_join";
        public static final String LOL4_START_VIDEO = "lol4_start_video";
        public static final String LOL4_END_VIDEO = "lol4_end_video";
        public static final String LOL4_GET_PRACTICE = "lol4_get_practice";
        public static final String LOL4_SUBMIT_PRACTICE = "lol4_submit_practice";
        public static final String NEXT_LOL4_AFTER_DONE_PRACTICE = "lol4_click_next_lol4";
        public static final String VIEW_PRACTICE_ANSWER_DETAIL = "lol4_click_button_view_detail_answer";
        public static final String DONE_LOL4 = "lol4_done";

    }

    public static class LOL6Event{
        public static final String JOIN_LOL6 = "lol6_click_join";
        public static final String LOL6_START_VIDEO = "lol6_start_video";
        public static final String LOL6_END_VIDEO = "lol6_end_video";
        public static final String LOL6_START_QUIZ = "lol6_start_quiz";
        public static final String LOL6_GET_QUIZ = "lol6_get_quiz";
        public static final String LOL6_SUBMIT_QUIZ = "lol6_submit_quiz";
        public static final String LOL6_END_QUIZ = "lol6_end_quiz";

    }
}
