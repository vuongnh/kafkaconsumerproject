package vn.studynow.learninghistoryservice.model;

import lombok.Data;
import lombok.experimental.SuperBuilder;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.io.Serializable;
import java.util.List;


@Data
@SuperBuilder
@Document(collection = "event_tracking")
public class Event extends CommonTracking implements Serializable {

    @Field(value = "event_name")
    private String eventName;

    @Field(value = "pre_code")
    private String preCode;

    @Field(value = "video_params")
    private List<VideoTracking> videoParams;

}
