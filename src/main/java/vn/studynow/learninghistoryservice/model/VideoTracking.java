package vn.studynow.learninghistoryservice.model;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Field;

@Data
@Builder
public class VideoTracking {

    @Field(value = "start_time")
    private int startTime;

    @Field(value = "pause_time")
    private int pauseTime;

    @Field(value = "resume_time")
    private int resumeTime;

    @Field(value = "rewind_time")
    private int rewindTime;

    @Field(value = "forward_time")
    private int forwardTime;

}
