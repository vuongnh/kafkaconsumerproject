package vn.studynow.learninghistoryservice.model.audit;

import lombok.Data;
import lombok.experimental.SuperBuilder;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.mongodb.core.mapping.Field;

import java.io.Serializable;
import java.time.Instant;

@Data
@SuperBuilder
public abstract class DateAudit implements Serializable {

    @CreatedDate
    @Field(value = "created_at")
    private Instant createdAt;

    @LastModifiedDate
    @Field(value = "updated_at")
    private Instant updatedAt;
}
