package vn.studynow.learninghistoryservice.model;

import lombok.Data;
import lombok.experimental.SuperBuilder;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Field;
import vn.studynow.learninghistoryservice.model.audit.DateAudit;

import java.io.Serializable;

@Data
@SuperBuilder
public abstract class CommonTracking extends DateAudit implements Serializable {

    @Id
    private String id;

    @Field(value = "code")
    private String code;

    @Field(value = "event_date")
    private String eventDate;

    @Field(value = "user_id")
    private String userId;

    @Field(value = "device_id")
    private String deviceId;

    @Field(value = "grade")
    private int grade;

    @Field(value = "subject_id")
    private int subjectId;

    @Field(value = "lol0_uid")
    private String cycleZeroUid;

    @Field(value = "lol1_uid")
    private String cycleOneUid;

    @Field(value = "lol2_uid")
    private String cycleTwoUid;

    @Field(value = "lol3_uid")
    private String cycleThreeUid;

    @Field(value = "lol4_uid")
    private String cycleFourUid;

    @Field(value = "lol6_1_uid")
    private String cycleSixDotOneUid;

    @Field(value = "lol6_2_uid")
    private String cycleSixDotTwoUid;

    @Field(value = "lol6_3_uid")
    private String cycleSixDotThreeUid;

    @Field(value = "version")
    private String version;

    @Field(value = "device_type")
    private String deviceType;

    @Field(value = "event_timestamp")
    private int eventTimestamp;

}
