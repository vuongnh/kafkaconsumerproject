package vn.studynow.learninghistoryservice.model;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Field;

@Data
@Builder
public class Quiz {

    @Field(value = "question_id")
    private String questionId;

    @Field(value = "question_image_url")
    private String questionImageUrl;

    @Field(value = "answer_image_url")
    private String answerImageUrl;

    @Field(value = "question_text")
    private String questionText;

    @Field(value = "answer_text")
    private String answerText;

    @Field(value = "correct_answer")
    private String correctAnswer;

    @Field(value = "user_answer")
    private String userAnswer;

    @Field(value = "time_answer")
    private int timeAnswer;

    @Field(value = "start_time")
    private String startTime;

    @Field(value = "end_time")
    private String endTime;

}
