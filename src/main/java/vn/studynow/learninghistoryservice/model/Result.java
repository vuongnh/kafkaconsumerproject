package vn.studynow.learninghistoryservice.model;

import lombok.Data;
import lombok.experimental.SuperBuilder;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.io.Serializable;
import java.util.List;

@Document(collection = "result_tracking")
@Data
@SuperBuilder
public class Result extends CommonTracking implements Serializable {


    @Field(value = "question_params")
    List<Quiz> questionParams;

    @Field(value = "total_time_answer")
    private int totalTimeAnswer;

    @Field(value = "num_answer")
    private int numAnswer;

    @Field(value = "num_question")
    private int numQuestion;

    @Field(value = "num_correct_answer")
    private int numCorrectAnswer;

    @Field(value = "point")
    private float point;

    @Field(value = "tracking_code")
    private String trackingCode;

}
